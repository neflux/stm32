FROM ubuntu:18.04

ENV DEBIAN_FRONTEND noninteractive 

# Install needed packages
RUN apt-get update && \
    apt-get install -y software-properties-common
RUN apt install build-essential -y --no-install-recommends
RUN apt-get install -y git
# Crazyflie
RUN add-apt-repository ppa:team-gcc-arm-embedded/ppa
RUN apt-get update
RUN apt install gcc-arm-embedded -y

# Clone Jerome version of Crazyflie firmware
RUN git clone --recursive https://github.com/jeguzzi/crazyflie-firmware/

WORKDIR /crazyflie-firmware
RUN git checkout ai-deck
RUN git checkout 5508edec7f81c01338560820c9e0eba99d1171d7

COPY fix.patch /fix.patch
RUN git apply /fix.patch

RUN make PLATFORM=cf2

# Not easy to forward the pyqt to the display server
# But this is still needed in order to make bitcraze firmware
RUN apt-get install -y libopencv-dev python3-pip
RUN pip3 install --upgrade pip
RUN pip3 install cfclient

WORKDIR /
RUN git clone --recursive https://github.com/bitcraze/crazyflie-firmware/ bitcraze-crazyflie-firmware
WORKDIR /bitcraze-crazyflie-firmware
# Need to select a stable version, otherwise it won't compile
RUN git checkout tags/2021.06
RUN make PLATFORM=cf2

WORKDIR /
RUN apt-get install -y x11-apps 
RUN apt-get install -y libxcb-xinerama0
